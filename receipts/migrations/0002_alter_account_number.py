# Generated by Django 4.1.5 on 2023-01-19 18:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("receipts", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="account",
            name="number",
            field=models.CharField(max_length=20),
        ),
    ]
